# SudoShow 40: 2021, A Year in Review

[Episode 40 Playback](https://sudo.show/40)

## Show Notes

### Episode Links
- [32 - Open Source Sustainability](https://sudo.show/32)
- [30 - Loving Your Work with Dashaun Carter](https://sudo.show/30)
- [27 - Open Source Virtual Desktop Infrastrcture](https://sudo.show/27)
- [16 - Starting a Home Lab](https://sudo.show/16)
- [18 - Managing Multi-Cloud with Chris Psaltis](https://sudo.show/18)
- [22 - Tidelift](https://sudo.show/22)
- [24 - Data Quality wiht Soda](https://sudo.show/24)
- [37 - Data Integration with Michel Tricot of Airbyte](https://sudo.show/37)
- [28 - Security Intelligence with Steve Ginty of RiskIQ](https://sudo.show/28)
- [35 - Busting Open Source Security Myths](https://sudo.show/35)



### Software Links
- [Project Hamster](https://github.com/projecthamster)

### Websites
- [Destination Linux Network](https://destinationlinux.network)
- [Sudo Show Website](https://sudo.show)

### Support the Show
- [Sponsor: Bitwarden](https://bitwarden.com/dln)
- [Sponsor: Digital Ocean](https://do.co/dln)
- [Sudo Show Swag](https://sudo.show/swag)

### Contact Us:
- [DLN Discourse](https://sudo.show/discuss)
- [Email Us!](mailto:contact@sudo.show)
- [Sudo Matrix Room](https://sudo.show/matrix)
- [Sudo Show GitLab](https://gitlab.com/sudoshow)

### Follow our Hosts:

- [Brandon's Website](https://open-tech.net)
- [Eric's Website](https://itguyeric.com)
- [Red Hat Streaming](https://www.redhat.com/en/livestreaming)


## Chapters

- 00:00  Intro
- 00:42  Welcome
- 02:41  DigitalOcean
- 03:45  Bitwarden
- 05:11  Main Content
- 47:20  Wrap Up
